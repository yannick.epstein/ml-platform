# ml-platform
Build for fun and out of interest. A platform to perfom machine learning: Store and query data sets, train, use and evaluate machine learning models. Focus is on automating the training, evaluation and publication of new models to have no hacky local code and more transparency as well as easier access to the models.
