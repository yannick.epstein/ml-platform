MINIO = 'minIO'

URL = 'url'
ACCESS_KEY = 'access_key'
ACCESS_SECRET = 'access_secret'

environments = {
    'minIO': {
        'url': 'http://172.27.0.1:9000',
        'access_key': 'training-data-minio',
        'access_secret': 'training-data-minio-secret'
    }
}
