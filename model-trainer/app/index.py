import falcon

from resources.model_resource import ModelResource
from resources.training_data_resource import TrainingDataResource

api = falcon.API()

model_resource = ModelResource()
api.add_route('/models', model_resource)
api.add_route('/models/{model_id}/{file_id}', model_resource)

data_resource = TrainingDataResource()
api.add_route('/trainingfiles', data_resource)
api.add_route('/trainingfiles/{id}', data_resource)
