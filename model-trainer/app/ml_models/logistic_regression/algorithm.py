from typing import Sequence, List

from pandas import DataFrame

from ml_models.types import Parameter

def parse(lines: Sequence) -> Sequence:
    return list(map(lambda line: 'Hello World!', lines))

def train(df: DataFrame, parameters: List[Parameter]=None) -> None:
    print(df, flush=True)
    model = [1, 2, 3, 4]
    print(model, flush=True)
    return model
