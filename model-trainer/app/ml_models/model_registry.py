from ml_models.types import ModelRegistry

from ml_models.logistic_regression import algorithm as logistic_regression

def get_models() -> ModelRegistry:
    return {
        'LR': logistic_regression
    }
