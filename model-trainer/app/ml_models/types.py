from typing import Any, Callable, Dict, List, Sequence

class Parameter:
    name: str
    value: Any

    def __init__(self, name: str, value: Any):
        self.name = name
        self.value = value

Algorithm = Callable[[Sequence, List[Parameter]], None]
ModelRegistry = Dict[str, Algorithm]
