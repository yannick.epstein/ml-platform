import json
import falcon

import services.model_training_service as training_service
import services.training_data_service as data_service

from ml_models import model_registry
class ModelResource:

    def on_get(self, req: falcon.Request, resp: falcon.Response) -> None:
        models = training_service.get_model_ids()
        resp.body = json.dumps(models, ensure_ascii=False)
        resp.status = falcon.HTTP_200

    def on_post(self, req: falcon.Request, resp: falcon.Response, model_id: str, file_id: str):
        parse = model_registry.get_models().get(model_id).parse
        training_df = data_service.get_file_content_as_df(file_id)
        resp.status = falcon.HTTP_202
        model = training_service.train_model(model_id, training_df)
