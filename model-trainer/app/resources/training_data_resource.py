from enum import Enum
import json
import falcon

import services.training_data_service as data_service

class QueryParameters(Enum):
    id: 'id'

class TrainingDataResource:

    def on_get(self, req: falcon.Request, resp: falcon.Response, id=None) -> None:
        print(id, flush=True)
        if id:
            self.build_file_content_resp(resp, id)
        else:
            self.build_file_ids_resp(resp)

    def build_file_content_resp(self, resp: falcon.Response, id: str):
        file_content = data_service.get_file_content(id)
        resp.body = json.dumps(file_content)
        resp.status = falcon.HTTP_200

    def build_file_ids_resp(self, resp: falcon.Response):
        ids = data_service.get_file_ids()
        resp.body = json.dumps(ids, ensure_ascii=False)
        resp.status = falcon.HTTP_200
