from typing import List, Sequence

from ml_models import model_registry

def get_model_ids() -> List[str]:
    return list(model_registry.get_models().keys())

def train_model(model_id: str, data_set: Sequence, parameters=None):
    return model_registry.get_models().get(model_id).train(data_set, parameters)
