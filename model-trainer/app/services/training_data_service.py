from typing import Callable, List, Sequence
import io
import json

import pandas as pd
from pandas import DataFrame

from minio import Minio
from minio.error import ResponseError

from environments.env import MINIO, URL, ACCESS_KEY, ACCESS_SECRET, environments

TRAINING_FILE_BUCKET = 'training-files'

minio_client = Minio(environments.get(MINIO).get(URL),
                access_key=environments.get(MINIO).get(ACCESS_KEY),
                secret_key=environments.get(MINIO).get(ACCESS_SECRET))

def get_file_ids() -> List[str]:
    return list(map(lambda object: object.key, minio_client.list_objects(TRAINING_FILE_BUCKET)))

def get_file_content(file_id: str) -> Sequence:
    data = minio_client.get_object(TRAINING_FILE_BUCKET, file_id)
    lines = list()
    for d in data:
        lines.append(str(d))
    return lines

def get_parsed_file_content(file_name: str, parse: Callable[[Sequence], Sequence]) -> Sequence:
    lines = get_file_content(file_name)
    return parse(lines)

def get_file_content_as_df(file_id: str) -> DataFrame:
    def decode(data) -> str:
        decoded = ''
        for content_block in data:
            decoded = decoded + content_block.decode('utf-8')
        return decoded

    def build_df(decoded: str) -> DataFrame:
        rows = decoded.split('\n')
        table = list()
        for row in rows:
            table.append(row.split(','))
        return pd.DataFrame(table)

    data = minio_client.get_object(TRAINING_FILE_BUCKET, file_id)
    decoded = decode(data)
    return build_df(decoded)
