import pytest
from unittest.mock import Mock

import services.model_training_service as training_service
import ml_models.model_registry as model_registry

def test_get_model_ids():
    expected_model_ids = list(model_registry.get_models().keys())
    model_ids = training_service.get_model_ids()
    assert model_ids == expected_model_ids
